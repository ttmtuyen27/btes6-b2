let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

//show list of glasses in the left
let showGlasses = (dataGlasses) => {
  let contentHTML = "";
  dataGlasses.forEach((glass) => {
    contentHTML += /*html*/ `
    <img id="${glass.id}" src="${glass.src}" class="col-4 my-2">
    `;
  });
  document.getElementById("vglassesList").innerHTML = contentHTML;
};

//show list of glasses when web start
showGlasses(dataGlasses);

//find index of glass
let findIndexOfGlass = (glassId) => {
  return dataGlasses.findIndex((item) => {
    return item.id == glassId;
  });
};

//add click event for each glass
dataGlasses.forEach((glass) => {
  document.getElementById(`${glass.id}`).addEventListener("click", function () {
    let index = findIndexOfGlass(glass.id);
    //information about glass
    let contentHTML = /*html*/ `
      <p id="glassId" class="d-none">${dataGlasses[index].id}</p>
      <h4>
        ${dataGlasses[index].name} - ${dataGlasses[index].brand}
        (${dataGlasses[index].color})
      </h4>
      <p class="bg-danger d-inline py-1 px-2 rounded">
        $${dataGlasses[index].price}
      </p>
      <p>${dataGlasses[index].description}</p>
    `;
    //show glass and information
    document.getElementById("glassesInfo").innerHTML = contentHTML;
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById(
      "avatar"
    ).innerHTML = `<img src="${dataGlasses[index].virtualImg}">`;
  });
});

//glass disappear
document.getElementById("before").addEventListener("click", () => {
  document.getElementById("avatar").innerHTML = "";
});

//glass appear
document.getElementById("after").addEventListener("click", () => {
  let id = document.getElementById("glassId").innerText;
  let index = findIndexOfGlass(id);
  document.getElementById(
    "avatar"
  ).innerHTML = `<img src="${dataGlasses[index].virtualImg}">`;
});
